#
# Be sure to run `pod lib lint GOMONetSDK.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'GOMONetSDK'
  s.version          = '0.2.0.2'
  s.summary          = 'A short description of GOMONetSDK.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://gitlab.com/gomoIOS/GOMONetSDK'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'zhangjiemin000' => 'zhangjiemin000@gmail.com' }
  s.source           = { :git => 'git@gitlab.com:gomoIOS/GOMONetSDK.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  #s.dependency 'JSONKit'

  s.dependency 'YYKit'
  #s.dependency 'MJExtension'

  s.dependency 'SAMKeychain'
  
#s.source_files = 'GOMONetSDK/Classes/**/*'
  
  s.subspec 'Core' do |c|
      c.source_files = 'GOMONetSDK/Classes/Core/**/*'
      #c.public_header_files = "GOMONetSDK/Classes/Core/GMNetworking/GMNetworking.h"
      c.public_header_files = "GOMONetSDK/Classes/Core/**/*.h"
  end
  s.libraries = 'resolv'
  # s.resource_bundles = {
  #   'GOMONetSDK' => ['GOMONetSDK/Assets/*.png']
  # }

# s.public_header_files = 'GOMONetSDK/Classes/Core/GMNetworking/GMNetworking.h'
end
