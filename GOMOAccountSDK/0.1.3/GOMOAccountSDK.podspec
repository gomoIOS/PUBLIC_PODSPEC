#
# Be sure to run `pod lib lint GOMOAccountSDK.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'GOMOAccountSDK'
  s.version          = '0.1.3'
  s.summary          = 'A short description of GOMOAccountSDK.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://gitlab.com/gomo_sdk/GOMOAccountSDK'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'dengnengwei' => 'dengnengwei@gomo.com' }
  s.source           = { :git => 'git@gitlab.com:gomo_sdk/GOMOAccountSDK.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.platform = :ios,'8.0'
  s.ios.deployment_target = '8.0'
  #s.ios.vendored_frameworks = 'GOMOAccountSDK/netSDK/ios/GOMONetSDK.framework' 
  # 依赖的第三方/自己的framework
  s.subspec 'Core' do |c|
      s.source_files = 'GOMOAccountSDK/Classes/Core/**/*'
      s.public_header_files = "GOMOAccountSDK/Classes/Core/**/*.h"
  end

  s.dependency 'GOMONetSDK'

  s.dependency 'YYKit'
  
  s.dependency 'SAMKeychain'
  
  # s.resource_bundles = {
  #   'GOMOAccountSDK' => ['GOMOAccountSDK/Assets/*.png']
  # }

  #
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
